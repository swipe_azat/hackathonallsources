pragma solidity ^0.4.0;

import './CryptoPortfolio.sol';
import './StringUtils.sol';

contract TransactionsBase {

    struct Transaction {
    address userPortfolioAddress;
    string baseCurrency;
    string marketCurrency;
    string action;

    uint baseAmount;
    uint marketAmount;
    uint timestamp;
    }

    Transaction[] transactions;

    function TransactionsBase() public {

    }

    function addTransaction(address userPortfolioAddress,
    string baseCurrency,
    string marketCurrency,
    string action, uint baseAmount, uint marketAmount, uint timestamp) external {
        transactions.push(Transaction(userPortfolioAddress, baseCurrency, marketCurrency, action, baseAmount, marketAmount, timestamp));

        CryptoPortfolio cryptoPortfolio = CryptoPortfolio(userPortfolioAddress);
        if (StringUtils.equals(action, "BUY")) {
            cryptoPortfolio.removeAssetAmount(baseCurrency, baseAmount);
            cryptoPortfolio.addAssetAmount(marketCurrency, marketAmount);
        }
        else {
            cryptoPortfolio.removeAssetAmount(marketCurrency, marketAmount);
            cryptoPortfolio.addAssetAmount(baseCurrency, baseAmount);
        }

    }
}
