const Web3 = require("web3");
const fs = require("fs");
const Tx = require('ethereumjs-tx')
const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

const contractsJson = fs.readFileSync('ContractsData.json', 'utf8');
contractsData = JSON.parse(contractsJson);

var account = "0x54fBCF26cEaBDCd41B02B60e80959E58cBFa09EB";
//TODO безопасность
var key = new Buffer('cf4e85de1e1f1c58c4dc1f56cfcc631abb889a72b6fd06bdec52adb4ae842e0b', 'hex')

var abi = contractsData["contracts"]["TransactionsBase.sol:TransactionsBase"].interface
var contractAddress = "0xC248E3eb778DF14b10067b1Ab5dD2ead4AaB614C";
var contract = new web3.eth.Contract(JSON.parse(abi), contractAddress);

var addressOfPortfolio = "0x63860F208cC6E3f0A79D05FB86f3AD983B53F9E7";
web3.eth.getGasPrice().then(function (gasPrice) {
        const gasPriceHex = web3.utils.toHex(gasPrice * 10);
        const gasLimitHex = web3.utils.toHex(3000000);
        web3.eth.getTransactionCount(account).then(function (nonce) {
            var tra = {
                gasPrice: gasPriceHex,
                gasLimit: gasLimitHex,
                data: contract.methods.addTransaction(addressOfPortfolio, "BTC", "ETH", "BUY", 10, 100, 123).encodeABI(),
                from: account,
                to:contractAddress,
                nonce: nonce
            };

            var tx = new Tx(tra);
            tx.sign(key);
            var stx = tx.serialize();
            web3.eth.sendSignedTransaction('0x' + stx.toString('hex'), function (err, hash) {
                if (err) {
                    console.log(err);
                    return;
                }
                console.log('contract creation tx: ' + hash);
            });
        });


    }
);



