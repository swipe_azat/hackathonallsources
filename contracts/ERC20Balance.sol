pragma solidity ^0.4.0;


contract ERC20Balance {
    function balanceOf(address who) constant returns (uint256);
}
