const solc = require("solc");
const fs = require("fs");
const Tx = require('ethereumjs-tx')

const cryptoPortfolioSource = fs.readFileSync('CryptoPortfolio.sol', 'utf8');
const stringUtilsSource = fs.readFileSync('StringUtils.sol', 'utf8');
const transactionsBaseSource = fs.readFileSync('TransactionsBase.sol', 'utf8');
const erc20 = fs.readFileSync('ERC20Balance.sol', 'utf8');

const input = {
    'CryptoPortfolio.sol': cryptoPortfolioSource,
    'StringUtils.sol': stringUtilsSource,
    'TransactionsBase.sol': transactionsBaseSource,
    'ERC20Balance.sol': erc20

}

const compiledContract = solc.compile({sources: input}, 1);
const json = JSON.stringify(compiledContract);
fs.writeFileSync('./ContractsData.json', json , 'utf-8');

console.log(compiledContract);


