const Web3 = require("web3");
const fs = require("fs");
const Tx = require('ethereumjs-tx')
const web3 = new Web3(new

Web3.providers.HttpProvider("http://localhost:8545"));//https://ropsten.infura.io/kzaSoDKYh7n4v1lKAU6

const contractsJson = fs.readFileSync('ContractsData.json', 'utf8');
contractsData = JSON.parse(contractsJson);

var account = "0x54fBCF26cEaBDCd41B02B60e80959E58cBFa09EB";
//TODO безопасность
var key = new Buffer('cf4e85de1e1f1c58c4dc1f56cfcc631abb889a72b6fd06bdec52adb4ae842e0b', 'hex')
var bytecode = "0x" + contractsData["contracts"]["CryptoPortfolio.sol:CryptoPortfolio"].bytecode;

web3.eth.getGasPrice().then(function (gasPrice) {
        const gasPriceHex = web3.utils.toHex(gasPrice * 100);
        const gasLimitHex = web3.utils.toHex(1000000);
        web3.eth.getTransactionCount(account).then(function (res) {

            var tra = {
                gasPrice: gasPriceHex,
                gasLimit: gasLimitHex,
                data: bytecode,
                from: account,
                nonce: res
            };
            var tx = new Tx(tra);
            tx.sign(key);
            var stx = tx.serialize();
            web3.eth.sendSignedTransaction('0x' + stx.toString('hex'), function (err, hash) {
                if (err) {
                    console.log(err);
                    return;
                }
                console.log('contract creation tx: ' + hash);
            });

        });
    }
);






