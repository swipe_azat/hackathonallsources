pragma solidity ^0.4.0;


import './StringUtils.sol';


//TODO SafeMath
contract CryptoPortfolio {
    uint userId;

    struct PortfolioItem {
    string asset;
    uint assetAddress;
    uint amount;
    }

    PortfolioItem[] items;

    function CryptoPortfolio() public {

    }

    function checkAssetExistance(string asset) external view returns (bool) {
        for (uint i = 0; i < items.length; i++) {
            if (StringUtils.equals(items[i].asset, asset)) {
                return true;
            }
        }
        return false;
    }

    function addNewAsset(string asset, uint assetAddress, uint amount) external {
        //TODO check existance?
        items.push(PortfolioItem(asset, assetAddress, amount));
    }


    function getAssetWalletAddress(string asset) external view returns (uint) {
        for (uint i = 0; i < items.length; i++) {
            if (StringUtils.equals(items[i].asset, asset)) {
                return items[i].assetAddress;
            }
        }
        return 0;
    }

    function getAssetWalletAmount(string asset) external view returns (uint) {
        for (uint i = 0; i < items.length; i++) {
            if (StringUtils.equals(items[i].asset, asset)) {
                return items[i].amount;
            }
        }
        return 0;
    }



    function addAssetAmount(string asset, uint amount) external {
        for (uint i = 0; i < items.length; i++) {
            if (StringUtils.equals(items[i].asset, asset)) {
                items[i].amount += amount;
                return;
            }
        }
    }

    function removeAssetAmount(string asset, uint amount) external {
        for (uint i = 0; i < items.length; i++) {
            if (StringUtils.equals(items[i].asset, asset)) {
                items[i].amount -= amount;
                return;
            }
        }
    }


}
