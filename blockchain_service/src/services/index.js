const portfolios = require('./portfolios/portfolios.service.js');
const deploy = require('./deploy/deploy.service.js');
const transactions = require('./transactions/transactions.service.js');

module.exports = function () {
  const app = this;
  app.configure(portfolios);
  app.configure(deploy);
  app.configure(transactions);

};
