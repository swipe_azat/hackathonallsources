const web3Lib = require("web3");
const fs = require("fs");
const Tx = require('ethereumjs-tx')

var account = "0x54fBCF26cEaBDCd41B02B60e80959E58cBFa09EB";
//TODO безопасность
var key = new Buffer('cf4e85de1e1f1c58c4dc1f56cfcc631abb889a72b6fd06bdec52adb4ae842e0b', 'hex')

var web3;
var contractsData;

function generateResponseJSON(res) {
  return {result: res}
}

function getContractAddressByTransaction(hash) {
  return new Promise(function (resolve, reject) {
    web3.eth.getTransactionReceipt(hash, onGetAddress);

    function onGetAddress(err, res) {

      if (res != null) {
        resolve(res.contractAddress);
      } else {
        reject("");
      }
    }
  });
}

function deployTransactionsBase() {
  return new Promise(function (resolve, reject) {
    var bytecode = "0x" + contractsData["contracts"]["TransactionsBase.sol:TransactionsBase"].bytecode;
    web3.eth.getGasPrice().then(function (gasPrice) {
        const gasPriceHex = web3.utils.toHex(gasPrice * 100);
        const gasLimitHex = web3.utils.toHex(1000000);
        web3.eth.getTransactionCount(account).then(function (res) {

          var tra = {
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            data: bytecode,
            from: account,
            nonce: res
          };
          var tx = new Tx(tra);
          tx.sign(key);
          var stx = tx.serialize();
          web3.eth.sendSignedTransaction('0x' + stx.toString('hex'), function (err, hash) {
            if (err) {
              console.log(err);
              return;
            }
            console.log('contract creation tx: ' + hash);
            resolve(hash);
          });
        });
      }
    );
  });

}

function deployPortfolio() {
  return new Promise(function (resolve, reject) {
    var bytecode = "0x" + contractsData["contracts"]["CryptoPortfolio.sol:CryptoPortfolio"].bytecode;

    web3.eth.getGasPrice().then(function (gasPrice) {
        const gasPriceHex = web3.utils.toHex(gasPrice * 100);
        const gasLimitHex = web3.utils.toHex(1000000);
        web3.eth.getTransactionCount(account).then(function (res) {

          var tra = {
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            data: bytecode,
            from: account,
            nonce: res
          };
          var tx = new Tx(tra);
          tx.sign(key);
          var stx = tx.serialize();
          web3.eth.sendSignedTransaction('0x' + stx.toString('hex'), function (err, hash) {
            if (err) {
              console.log(err);
              return;
            }
            console.log('contract creation tx: ' + hash);
            resolve(hash);
          });

        });
      }
    );
  });

}



class Service {

  constructor(options) {

    console.log(web3Lib.version);
    web3 = new web3Lib(new web3Lib.providers.HttpProvider(options.web3Provider));
    const contractsJson = fs.readFileSync('ContractsData.json', 'utf8');
    contractsData = JSON.parse(contractsJson);
  }


  find(params) {

    try {
      if (params.query.portfolio != undefined) {
        return deployPortfolio()
      }

      if (params.query.transactions != undefined) {
        return deployTransactionsBase()
      }

      if (params.query.getContractAddress != undefined) {
        return getContractAddressByTransaction(params.query.hash)
      }


    } catch(e) {
      Promise.resolve(e);
    }


    return Promise.resolve({});

  }

}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
