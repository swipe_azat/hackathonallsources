// Initializes the `balances` service on path `/balances`
const createService = require('./deploy.class.js');

module.exports = function () {
  const app = this;

  const options = {
    name: 'deploy',
    web3Provider: app.get("web3_provider")
  };

  app.use('/deploy', createService(options));

};
