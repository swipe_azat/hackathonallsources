const web3Lib = require("web3");
const fs = require("fs");
const Tx = require('ethereumjs-tx')

var web3;
var contractsData;

function generateResponseJSON(res) {
  return {result: res}
}

function addAsset(portfolioAddress,  asset, assetAddress, amount) {
  return new Promise(function (resolve, reject) {
    var abi = contractsData["contracts"]["CryptoPortfolio.sol:CryptoPortfolio"].interface
    var contract = new web3.eth.Contract(JSON.parse(abi), portfolioAddress);
    var account = "0x54fBCF26cEaBDCd41B02B60e80959E58cBFa09EB";

    web3.eth.getGasPrice().then(function (gasPrice) {
        const gasPriceHex = web3.utils.toHex(gasPrice * 10);
        const gasLimitHex = web3.utils.toHex(3000000);
        web3.eth.getTransactionCount(account).then(function (nonce) {
          var tra = {
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            data: contract.methods.addNewAsset(asset, assetAddress, amount).encodeABI(),
            from: account,
            to: contractAddress,
            nonce: nonce
          };

          var tx = new Tx(tra);
          tx.sign(key);
          var stx = tx.serialize();
          web3.eth.sendSignedTransaction('0x' + stx.toString('hex'), function (err, hash) {
            if (err) {
              console.log(err);
              return;
            }
            console.log('contract creation tx: ' + hash);
            resolve(hash);
          });
        });
      }
    );
  });
}

function addAssetAmount(portfolioAddress, asset, amount) {
  return new Promise(function (resolve, reject) {
    var abi = contractsData["contracts"]["CryptoPortfolio.sol:CryptoPortfolio"].interface
    var contract = new web3.eth.Contract(JSON.parse(abi), portfolioAddress);
    web3.eth.getGasPrice().then(function (gasPrice) {
        const gasPriceHex = web3.utils.toHex(gasPrice * 10);
        const gasLimitHex = web3.utils.toHex(3000000);
        web3.eth.getTransactionCount(account).then(function (nonce) {
          var tra = {
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            data: contract.methods.addAssetAmount(asset, amount).encodeABI(),
            from: account,
            to: contractAddress,
            nonce: nonce
          };

          var tx = new Tx(tra);
          tx.sign(key);
          var stx = tx.serialize();
          web3.eth.sendSignedTransaction('0x' + stx.toString('hex'), function (err, hash) {
            if (err) {
              console.log(err);
              return;
            }
            console.log('contract creation tx: ' + hash);
            resolve(hash);
          });
        });
      }
    );
  });
}

class Service {

  constructor(options) {

    web3 = new web3Lib(new web3Lib.providers.HttpProvider(options.web3Provider));
    const contractsJson = fs.readFileSync('ContractsData.json', 'utf8');
    contractsData = JSON.parse(contractsJson);
  }


  find(params) {

    if (params.query.addAsset != undefined) {
      return addAsset(params.query.portfolioAddress, params.query.asset, params.query.assetAddress, amount)
    }

    if (params.query.addAssetAmount != undefined) {
      return addAssetAmount(params.query.portfolioAddress, params.query.asset, params.query.assetAddress, amount)
    }

    return Promise.resolve({});
  }

}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
