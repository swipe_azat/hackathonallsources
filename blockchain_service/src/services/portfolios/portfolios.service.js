// Initializes the `balances` service on path `/balances`
const createService = require('./portfolios.class.js');

module.exports = function () {
  const app = this;

  const options = {
    name: 'portfolios',
    web3Provider: app.get("web3_provider")
  };

  app.use('/portfolios', createService(options));

};
