// Initializes the `exchange_rates` service on path `/exchange-rates`
const createService = require('./transactions.class.js');

module.exports = function () {
  const app = this;

  const options = {
    name: 'transactions',
    web3Provider: app.get("web3_provider")
  };

  app.use('/transactions', createService(options));

};
