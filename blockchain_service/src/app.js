const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const error = require('feathers-errors/handler');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const rest = require('feathers-rest');

const middleware = require('./middleware');
const services = require('./services');


const app = feathers();

app.configure(configuration());
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.configure(rest());

app.configure(middleware);
app.configure(services);
app.use(error({
  html:false,
  json: function (error, req, res, next) {
    res.send("{\"error\":\"" + error.message + "\"}");
  }
}));

module.exports = app;
