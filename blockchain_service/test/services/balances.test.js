const assert = require('assert');
const app = require('../../src/app');

describe('\'portfolios\' service', () => {
  it('registered the service', () => {
    const service = app.service('balances');

    assert.ok(service, 'Registered the service');
  });
});
